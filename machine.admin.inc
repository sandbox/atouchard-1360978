<?php
// $Id$

/**
 * @file
 * Admin page callback file for the machine module.
 */

/**
 * Theme the administer permissions page.
 * @param unknown_type $form
 */
function theme_machine_admin_perm($form) {

  // Get existing roles and permissions
  $roles = variable_get('machine_roles', NULL);
  $permissions = variable_get('machine_permissions', NULL);

  $output = '';

  // Build header
  $header = array(
    t('Permission'),
  );
  foreach ($roles as $role) {
    $header[] = $role;
  }

  // Build datas
  $rows = array();
  foreach ($permissions as $permission) {
    $row = array('<em>' . $permission . '</em>');
    foreach ($roles as $role) {
      $row[] = drupal_render($form['checkboxes'][$role][$permission]);
    }
    $rows[] = $row;
  }

  $output .= theme('table', $header, $rows);
  $output .= drupal_render($form['submit']);
  $output .= drupal_render($form);

  return $output;
}

/**
 * Menu callback: administer permissions.
 */
function machine_admin_perm($form_state) {

  $form = array();

  // Get existing roles and permissions
  $roles = variable_get('machine_roles', NULL);
  $permissions = variable_get('machine_permissions', NULL);
  $roles_permissions = variable_get('machine_roles_permissions', NULL);

  // Browse existing roles to build checkboxes
  foreach ($roles as $role) {
    $options = array();
    // Browse existing permission to build form
    foreach ($permissions as $permission) {
      $options[$permission] = '';
      foreach ($roles_permissions[$role] as $existing_perm) {
        if ($existing_perm == $permission) {
          $status[$role][] = $permission;
        }
      }
    }
  }

  // Have to build checkboxes here after checkbox arrays are built
  foreach ($roles as $role) {
    $form['checkboxes'][$role] = array('#type' => 'checkboxes', '#options' => $options, '#default_value' => isset($status[$role]) ? $status[$role] : array());
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save permissions'),
  );

  return $form;
}

/**
 * Submit for permission form.
 */
function machine_admin_perm_submit($form, &$form_state) {
  // Get existing roles and permissions
  $roles = variable_get('machine_roles', NULL);
  $permissions = variable_get('machine_permissions', NULL);

  $roles_permissions = array();

  if ($roles != NULL && $permissions != NULL) {
    // Browse submitted permissions
    foreach ($roles as $role) {
      $roles_permissions[$role] = array();
      foreach ($permissions as $permission) {
        if ($form_state['values'][$role][$permission]) {
          $roles_permissions[$role][] = $permission;
        }
      }
    }
  }

  // Set variables
  variable_set('machine_roles_permissions', $roles_permissions);

}